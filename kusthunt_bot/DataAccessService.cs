﻿using Mongo.CRUD;
using Mongo.CRUD.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;

namespace kurshunt_bot
{
    public class DataAccessService
    {
        private MongoCRUD<BackendCourse> backend;
        private MongoCRUD<FrontendCourse> frontend;

        public DataAccessService()
        {
            backend = new MongoCRUD<BackendCourse>(
                "mongodb://dima:1234abc@ds243931.mlab.com:43931/course-hunters-bot-db",
                "course-hunters-bot-db"
            );

            frontend = new MongoCRUD<FrontendCourse>(
                "mongodb://dima:1234abc@ds243931.mlab.com:43931/course-hunters-bot-db",
                "course-hunters-bot-db"
            );
        }

        public ICourse GetCourse(string key)
        {
            var id = ObjectId.Parse(key);

            return backend.Get(id);
        }

        public long GetCoursesCount(Collection collection)
        {
            switch (collection)
            {
                case Collection.Frontend:
                    return GetFrontendCoursesCount();

                case Collection.Backend:
                    return GetBackendCoursesCount();

                default:
                    return -1;
            }
        }

        public IEnumerable<ICourse> GetCourses(Collection collection)
        {
            switch (collection)
            {
                case Collection.Frontend:
                    return GetFrontendCourses();

                case Collection.Backend:
                    return GetBackendCourses();

                default:
                    return null;
            }
        }

        public IEnumerable<ICourse> GetCourses(Collection collection, int page, int pageSize)
        {
            switch (collection)
            {
                case Collection.Frontend:
                    return GetFrontendCourses(page, pageSize);

                case Collection.Backend:
                    return GetBackendCourses(page, pageSize);

                default:
                    return null;
            }
        }

        public long GetFrontendCoursesCount()
        {
            var result = frontend.Search(new FilterDefinitionBuilder<FrontendCourse>().Empty);

            return result.Count;
        }

        public long GetBackendCoursesCount()
        {
            var result = backend.Search(new FilterDefinitionBuilder<BackendCourse>().Empty);

            return result.Count;
        }

        public IEnumerable<ICourse> GetFrontendCourses()
        {
            var result = frontend.Search(new FilterDefinitionBuilder<FrontendCourse>().Empty);

            var data = result.Documents.ToList();

            return data;
        }

        public IEnumerable<ICourse> GetFrontendCourses(int page, int pageSize)
        {
            var options = new SearchOptions
            {
                PageNumber = page,
                PageSize = pageSize
            };

            var result = frontend.Search(new FilterDefinitionBuilder<FrontendCourse>().Empty, options);

            var data = result.Documents.ToList();

            return data;
        }

        public IEnumerable<ICourse> GetBackendCourses()
        {
            var result = backend.Search(new FilterDefinitionBuilder<BackendCourse>().Empty);

            var data = result.Documents.ToList();

            return data;
        }

        public IEnumerable<ICourse> GetBackendCourses(int page, int pageSize)
        {
            var options = new SearchOptions {
                PageNumber = page,
                PageSize = pageSize
            };

            var result = backend.Search(new FilterDefinitionBuilder<BackendCourse>().Empty, options);

            var data = result.Documents.ToList();

            return data;
        }
    }

    public enum Collection
    {
        Frontend,
        Backend
    }
}
