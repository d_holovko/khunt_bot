﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace kurshunt_bot
{
    public class KHunterBot
    {
        private const int PAGE_SIZE = 5;

        private ITelegramBotClient botClient;
        private DataAccessService dataAccessService;

        public KHunterBot()
        {
            botClient = new TelegramBotClient("649458062:AAFh23enkt4pWAxO9P29CljtQK6kVi-10aI");

            var me = botClient.GetMeAsync().Result;
            // botClient.SendTextMessageAsync(chatId: 382418668, text: "I am alive!");            

            botClient.OnMessage += OnMessageReceived;

            botClient.OnCallbackQuery += OnCallbackReceived;

            dataAccessService = new DataAccessService();

            botClient.StartReceiving();
        }        

        public async void OnMessageReceived(object sender, MessageEventArgs e)
        {
            await InitialMenu(sender, e);
        }        

        private async void OnCallbackReceived(object sender, CallbackQueryEventArgs e)
        {
            var query = e.CallbackQuery.Data;

            if (query.StartsWith("coursesPage:"))
            {
                await CoursesMenu(Collection.Backend, e);
                return;
            }

            if (query.StartsWith("courseId:"))
            {
                await LessonsMenu(sender, e);
                return;
            }

            switch (query)
            {
                case "Search":
                case "Frontend":
                    await CoursesMenu(Collection.Frontend, e);
                    return;
                case "Backend":
                    await CoursesMenu(Collection.Backend, e);
                    return;
            }            
        }

        private async Task InitialMenu(object sender, MessageEventArgs e)
        {
            await botClient.SendTextMessageAsync(
                chatId: e.Message.Chat,
                text: "Choose",
                disableNotification: true,
                replyMarkup: new InlineKeyboardMarkup(new List<InlineKeyboardButton> {
                    InlineKeyboardButton.WithCallbackData("Frontend"),
                    InlineKeyboardButton.WithCallbackData("Search"),
                    InlineKeyboardButton.WithCallbackData("Backend")
                })
            );
        }

        public async Task CoursesMenu(Collection collection, CallbackQueryEventArgs e)
        {
            bool isPageSwitch = false;

            var arguments = e.CallbackQuery.Data.Split(';');

            int pageNumber = 1;
            int pageCount = (int)Math.Ceiling(dataAccessService.GetCoursesCount(collection) / (double) PAGE_SIZE);

            if(arguments[0].StartsWith("coursesPage:"))
            {
                pageNumber = int.Parse(e.CallbackQuery.Data.Replace("coursesPage:", string.Empty));
                isPageSwitch = true;
            }

            if (pageNumber < 1)
            {
                pageNumber = pageCount;
            }
            else if(pageNumber > pageCount)
            {
                pageNumber = 1;
            }

            IEnumerable<ICourse> courses;

            var keyboard = new List<InlineKeyboardButton>();

            var messageText = new StringBuilder().AppendLine($"Page {pageNumber} of {pageCount}");

            courses = dataAccessService.GetCourses(collection, pageNumber, PAGE_SIZE);

            short cnt = 1;
            foreach (var course in courses)
            {
                messageText.AppendLine($"{cnt}) *{course.Title}* by _{course.Source}_ | {course.Language} | {course.NumberOfLessons } lessons | {course.Duration}");

                keyboard.Add(InlineKeyboardButton.WithCallbackData($"{cnt++}", $"courseId:{course.Key}"));
            }

            var pagingButtons = new List<InlineKeyboardButton> {
                InlineKeyboardButton.WithCallbackData("Previous page", $"coursesPage:{pageNumber - 1}"),
                InlineKeyboardButton.WithCallbackData("Next page", $"coursesPage:{pageNumber + 1}")
            };

            try
            {
                if (isPageSwitch)
                {
                    await EditMenuAsync(e, messageText, keyboard, pagingButtons);
                }
                else
                {
                    await SendMenuAsync(e, messageText, keyboard, pagingButtons);
                }
            }
            catch (MessageIsNotModifiedException exception)
            {
                // такой бесполезный эксепшн
                // еще и вылетает непонятно почему когда сообщение таки было изменено
            }
        }

        public async Task LessonsMenu(object sender, CallbackQueryEventArgs e)
        {
            bool isPageSwitch = false;

            var arguments = e.CallbackQuery.Data.Split(';');

            int pageNumber = 1;
            int pageCount = 1;

            var courseId = arguments[0].Replace("courseId:", string.Empty);

            if (arguments.Length > 1 && arguments[1].StartsWith("lessonsPage:"))
            {
                pageNumber = int.Parse(arguments[1].Replace("lessonsPage:", string.Empty));
                isPageSwitch = true;
            }

            var course = dataAccessService.GetCourse(courseId);            

            IEnumerable<Lesson> lessons;

            var messageText = new StringBuilder()
                .AppendLine($"*Title: {course.Title}* | _{course.Language}_")
                .AppendLine($"*Author:* {course.Source}")
                .AppendLine($"*Amount of lessons:* {course.NumberOfLessons}")
                .AppendLine($"*Total duration:* {course.Duration}")
                .AppendLine(course.BriefDescription)
                .AppendLine("*Lessons:*");

            if (course.NumberOfLessons > 5)
            {
                pageCount = (int)Math.Ceiling(course.NumberOfLessons / (double)PAGE_SIZE);

                if (pageNumber < 1)
                {
                    pageNumber = pageCount;
                }
                else if (pageNumber > pageCount)
                {
                    pageNumber = 1;
                }

                lessons = course.Lessons.Skip((pageNumber - 1) * PAGE_SIZE).Take(PAGE_SIZE);
                
                messageText.AppendLine($"Page {pageNumber} of {pageCount}");
            }
            else
            {
                lessons = course.Lessons;
            }            

            var keyboard = new List<InlineKeyboardButton>();

            short cnt = 1;
            foreach (var lesson in lessons)
            {
                messageText.AppendLine($"{cnt}) *{lesson.Title}* | {lesson.Duration}");

                keyboard.Add(InlineKeyboardButton.WithUrl($"{cnt++}", lesson.Url));
            }

            var pagingButtons = new List<InlineKeyboardButton> {
                InlineKeyboardButton.WithCallbackData("Previous page", $"courseId:{course.Key};lessonsPage:{pageNumber - 1}"),
                InlineKeyboardButton.WithCallbackData("Next page", $"courseId:{course.Key};lessonsPage:{pageNumber + 1}")
            };

            try
            {
                if (isPageSwitch)
                {
                    await EditMenuAsync(e, messageText, keyboard, pagingButtons);
                }
                else
                {
                    await SendMenuAsync(e, messageText, keyboard, pagingButtons);
                }
            }
            catch (MessageIsNotModifiedException exception)
            {
                // такой бесполезный эксепшн
                // еще и вылетает непонятно почему когда сообщение таки было изменено
            }                       
        }

        public Task<Message> EditMenuAsync(
            CallbackQueryEventArgs e, 
            StringBuilder messageText, 
            IEnumerable<InlineKeyboardButton> selectionButtons,
            IEnumerable<InlineKeyboardButton> pagingButtons
        )
        {
            return botClient.EditMessageTextAsync(
                chatId: e.CallbackQuery.Message.Chat,
                messageId: e.CallbackQuery.Message.MessageId,
                text: messageText.ToString(),
                parseMode: ParseMode.Markdown,
                replyMarkup: new InlineKeyboardMarkup(
                    new List<IEnumerable<InlineKeyboardButton>> { selectionButtons, pagingButtons })
            );
        }

        public Task<Message> SendMenuAsync(
            CallbackQueryEventArgs e,
            StringBuilder messageText,
            IEnumerable<InlineKeyboardButton> selectionButtons,
            IEnumerable<InlineKeyboardButton> pagingButtons
        )
        {
            return botClient.SendTextMessageAsync(
                chatId: e.CallbackQuery.Message.Chat,
                text: messageText.ToString(),
                parseMode: ParseMode.Markdown,
                disableNotification: true,
                replyMarkup: new InlineKeyboardMarkup(
                    new List<IEnumerable<InlineKeyboardButton>> { selectionButtons, pagingButtons })
            );
        }
    }
}
