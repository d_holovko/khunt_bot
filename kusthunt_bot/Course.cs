﻿using Mongo.CRUD.Attributes;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kurshunt_bot
{
    public interface ICourse
    {
        ObjectId Key { get; set; }

        string Title { get; set; }

        string ImageLink { get; set; }

        string BriefDescription { get; set; }

        string Source { get; set; }

        string Language { get; set; }

        int NumberOfLessons { get; set; }

        string Duration { get; set; }

        IEnumerable<Lesson> Lessons { get; set; }
    }

    [CollectionName("BackendCourses")]
    public class BackendCourse : ICourse
    {
        [BsonId]
        public ObjectId Key { get; set; }

        public string Id { get; set; }

        public string Title { get; set; }

        public string ImageLink { get; set; }

        public string BriefDescription { get; set; }

        public string Source { get; set; }

        public string Language { get; set; }

        public int NumberOfLessons { get; set; }

        public string Duration { get; set; }

        [BsonElement("LessonsLinks")]
        public IEnumerable<Lesson> Lessons { get; set; }
    }

    [CollectionName("FrontendCourses")]
    public class FrontendCourse : ICourse
    {
        [BsonId]
        public ObjectId Key { get; set; }

        public string Title { get; set; }

        public string ImageLink { get; set; }

        public string BriefDescription { get; set; }

        public string Source { get; set; }

        public string Language { get; set; }

        public int NumberOfLessons { get; set; }

        public string Duration { get; set; }

        [BsonElement("LessonsLinks")]
        public IEnumerable<Lesson> Lessons { get; set; }
    }

    public class Lesson
    {
        public string Title { get; set; }

        public string Url { get; set; }

        public string Duration { get; set; }
    }
}
